<?php

namespace App\Controller;

use App\Entity\Lot;

use App\Form\LotType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CreationLotController extends AbstractController
{
    /**
     * @Route("/creation/lot", name="app_creation_lot")
     */
    public function Ajouter(Request $request)
    {
        $lot = new Lot();
        $form = $this->createForm(LotType::class, $lot);
        $form->handleRequest($request);

        if($form->isSubmitted()&& $form->isValid()){

            $em=$this->getDoctrine()->getManager();
            $em->persist($lot);
            $em->flush();

            return $this->redirectToRoute('app_ajoutproduit');
        }


        return $this->render('creation_lot/index.html.twig', [
            'LotForm' => $form->createView(),
        ]);
    }
}
