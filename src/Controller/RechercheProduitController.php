<?php

namespace App\Controller;

use App\Entity\Produit;
use App\Form\SearchFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class RechercheProduitController extends AbstractController
{
    /**
     * @Route("/recherche/produit", name="app_recherche_produit")
     */
    public function searchProducts(Request $request): Response
    {
        $repo=$this->getDoctrine()->getRepository(Produit::class);


        $form = $this->createForm(SearchFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $repo = $this->getDoctrine()->getRepository(Produit::class);


            $produit=$repo->findByData($form->getData()['champ']);
        }
        else
        {
                $produit=$repo->findAll();
        }

        return $this->render('recherche_produit/index.html.twig', [
            'form' => $form->createView(),
            'produit'=>$produit,
        ]);
    }
    /**
     * @Route("/{id}", name="produit_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Produit $produit): Response
    {
        if ($this->isCsrfTokenValid('delete'.$produit->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($produit);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_recherche_produit');
    }
}
