<?php

namespace App\Controller;

use App\Entity\Enchere;
use App\Form\AjoutEnchereType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EnchereController extends AbstractController
{
    /**
     * @Route("/enchere", name="app_enchere")
     */
    public function visualiserEnchere(): Response
    {


        return $this->render('enchere/index.html.twig', [
            'controller_name' => 'EnchereController',
        ]);
    }

    /**
     * @Route ("/enchere/AjouterEnchere", name="app_ajout_enchere")
     */
    public function ajoutEnchere(Request $request)
    {
        $enchere = new Enchere();
        $form = $this->createForm(AjoutEnchereType::class, $enchere);
        $form->handleRequest($request);


        if($form->isSubmitted()&& $form->isValid())
        {
            $em=$this->getDoctrine()->getManager();
            $em->persist($enchere);
            $em->flush();

            return $this->redirectToRoute('app_enchere');
        }
        return $this->render('enchere/ajoutenchere.html.twig', [
            'ajoutenchereForm' => $form->createView(),
        ]);
    }
}
