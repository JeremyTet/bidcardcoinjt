<?php

namespace App\Controller;

use App\Entity\Produit;
use App\Form\AjouterProduitType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

class AjoutProduitController extends AbstractController
{
    /**
     * @Route("/ajout/produit", name="app_ajoutproduit")
     */
    public function ajouter(Request $request, SluggerInterface $slugger)
    {
        $produit = new Produit();

        $form = $this->createForm(AjouterProduitType::class, $produit);
        $form->handleRequest($request);

        if($form->isSubmitted()&& $form->isValid()){

            $imageFile =$form->get('image')->getData();

            if($imageFile){
                $originalFileName = pathinfo($imageFile->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = $slugger->slug($originalFileName);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$imageFile->guessExtension();

                //On bouge le fichier dans un endroit dédié
                try{
                    $imageFile->move(
                        $this->getParameter('image_produit'),
                        $newFilename
                    );
                } catch (FileException $e){
                    //mettre un message si ça marche pas
                }
                $produit->setImage($newFilename);
                //a test si modifier
                // new File($this->getParameter('brochures_directory').'/'.$product->getBrochureFilename()
            }

            $em=$this->getDoctrine()->getManager();
            $em->persist($produit);
            $em->flush();

            return $this->redirectToRoute('app_homepage');
        }



        return $this->render('ajout_produit/index.html.twig', [
            'AjouterProduitForm' => $form->createView(),
        ]);
    }
}
