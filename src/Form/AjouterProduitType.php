<?php

namespace App\Form;

use App\Entity\Categorie;
use App\Entity\Lot;
use App\Entity\Produit;
use App\Repository\CategorieRepository;
use App\Repository\LotRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AjouterProduitType extends AbstractType
{
    private $recupererLot;
    private $recupererCategorie;

    public function __construct(LotRepository $recupererLot,CategorieRepository $recupererCategorie)
    {
        $this->recupererLot = $recupererLot;
        $this->recupererCategorie= $recupererCategorie;
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom',TextType::class,[
                'label'=>"Nom du produit...",
                'required'=>true,
            ])
            ->add('Prix',TextType::class,[
                'label'=>"Prix du produit...",
                'required'=>true,
            ])
            ->add('image',FileType::class)
            ->add('lot', EntityType::class,[

                'class'=>Lot::class,
                'choice_label'=>'Nom',
                'choices'=> $this->recupererLot->findAll(),
            ])
            ->add('categorie', EntityType::class,[
                'class'=>Categorie::class,
                'choice_label'=>'Nom',
                'choices'=> $this->recupererCategorie->findAll(),
            ])
            ->add("OK",SubmitType::class,["label"=>"Enregistrer"])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Produit::class,
        ]);
    }
}
