<?php

namespace App\Form;

use App\Entity\Enchere;
use App\Entity\Lot;
use App\Repository\LotRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AjoutEnchereType extends AbstractType
{
    private $lotRepository;

    public function __construct(LotRepository $lotRepository)
    {
        $this->lotRepository = $lotRepository;

    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('montant',TextType::class,[
                'label'=>"Prix d'enchere...",
                'required'=>true,
            ])
            ->add('date',DateType::class, [
                'widget' => 'choice'])
            ->add('lot', EntityType::class,[
                'class'=>Lot::class,
                'choice_label'=>'Nom',
                'choices'=> $this->lotRepository->findAll(),
            ])
            ->add("OK",SubmitType::class,["label"=>"Enregistrer"])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Enchere::class,
        ]);
    }
}
